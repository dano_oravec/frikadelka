WIDTH = 1200
HEIGHT = 800

PLAYER_W = 30
PLAYER_H = 30
PLAYER_SPEED = 6
MAX_COLLECTED = 3

MAKARONI_NA_VIHRU = 3

MAKARONA_SIDE = 15

#  UP = 1, RIGHT = 2, DOWN = 4, LEFT = 8
# DX = [0,  0, 1,  1, 0, 0, 1, 0, -1, -1, 0, 0, -1, 0, 0, 0]
# DY = [0, -1, 0, -1, 1, 0, 1, 0,  0, -1, 0, 0,  1, 0, 0, 0]
DX = [0,  0, 1,  6, 0, 0, 6, 0, -1, -6, 0, 0, -6, 0, 0, 0]
DY = [0, -1, 0, -1, 1, 0, 1, 0,  0, -1, 0, 0,  1, 0, 0, 0]
SHOT_SPEED = [0, 11, 0, 2, 11, 0, 2, 0, 11, 2, 0, 0, 2, 0, 0, 0]
SHOT_W = 10
SHOT_H = 10

HRNIEC_W = 10
HRNIEC_H = HEIGHT / 16
HRNCE_SPACE = 20

ZONA_W = WIDTH / 5

FONT_SIZE = 50
