import pygame
from frikadelka.constants import *
from random import randrange
import time


class Rectangle:
    def __init__(self, _x1, _y1, _x2, _y2):
        self.x1 = _x1
        self.y1 = _y1
        self.x2 = _x2
        self.y2 = _y2
        self.ul_corner = [self.x1, self.y1]
        self.ur_corner = [self.x2, self.y1]
        self.ll_corner = [self.x1, self.y2]
        self.lr_corner = [self.x2, self.y2]
        self.corners = [self.ul_corner, self.ur_corner, self.ll_corner, self.lr_corner]


class Missile:
    def __init__(self, _x, _y, _pid, _direction, _speedx, _speedy, _width, _height, _colour):
        self.x = _x
        self.y = _y
        self.pid = _pid
        self.direction = _direction
        self.speedx = _speedx
        self.speedy = _speedy
        self.width = _width
        self.height = _height
        self.colour = _colour
        self.rect = Rectangle(self.x, self.y, self.x + self.width, self.y + self.height)

    def move(self):
        self.x += self.speedx * DX[self.direction]
        self.y += self.speedy * DY[self.direction]
        self.rect = Rectangle(self.x, self.y, self.x + self.width, self.y + self.height)


def collide(rect1, rect2):
    rects = [rect1, rect2]
    for i in range(2):
        outer = rects[(i + 1) % 2]
        for c in rects[i % 2].corners:
            if outer.x1 <= c[0] <= outer.x2 and outer.y1 <= c[1] <= outer.y2:
                return True
    return False


attack_zone = Rectangle(0, 0, ZONA_W, HEIGHT)


class Player:
    def __init__(self, _id, _x, _y):
        self.id = _id
        self.width = PLAYER_W
        self.height = PLAYER_H
        self.collected = 0  # number of collected makaroni
        self.x = _x
        self.y = _y
        self.color = (160, 0, 0) if self.id == 1 else (0, 160, 0)
        self.missiles = []
        self.score = 0
        self.rect = Rectangle(self.x, self.y, self.x + self.width, self.y + self.height)

    @staticmethod
    def try_to_move(X, Y, nX, nY):
        if 0 <= nX <= WIDTH - PLAYER_W - ZONA_W and 0 <= nY <= HEIGHT - PLAYER_H:
            return nX, nY
        return X, Y

    def move(self):
        pressed = pygame.key.get_pressed()
        if self.id == 1:
            if pressed[pygame.K_UP]:
                self.x, self.y = self.try_to_move(self.x, self.y, self.x, self.y - PLAYER_SPEED)
            if pressed[pygame.K_DOWN]:
                self.x, self.y = self.try_to_move(self.x, self.y, self.x, self.y + PLAYER_SPEED)
            if pressed[pygame.K_LEFT]:
                self.x, self.y = self.try_to_move(self.x, self.y, self.x - PLAYER_SPEED, self.y)
            if pressed[pygame.K_RIGHT]:
                self.x, self.y = self.try_to_move(self.x, self.y, self.x + PLAYER_SPEED, self.y)
        else:
            if pressed[pygame.K_w]:
                self.x, self.y = self.try_to_move(self.x, self.y, self.x, self.y - PLAYER_SPEED)
            if pressed[pygame.K_s]:
                self.x, self.y = self.try_to_move(self.x, self.y, self.x, self.y + PLAYER_SPEED)
            if pressed[pygame.K_a]:
                self.x, self.y = self.try_to_move(self.x, self.y, self.x - PLAYER_SPEED, self.y)
            if pressed[pygame.K_d]:
                self.x, self.y = self.try_to_move(self.x, self.y, self.x + PLAYER_SPEED, self.y)
        self.rect = Rectangle(self.x, self.y, self.x + self.width, self.y + self.height)

    def shoot(self):
        if self.collected == 0:
            return
        back_only = False
        if not collide(attack_zone, Rectangle(self.x, self.y, self.x + self.width, self.y + self.height)):
            back_only = True
        pressed = pygame.key.get_pressed()
        mask = 0
        if self.id == 1:
            if pressed[pygame.K_UP]:
                mask += 1
            if pressed[pygame.K_DOWN]:
                mask += 4
            if pressed[pygame.K_LEFT]:
                mask += 8
            if pressed[pygame.K_RIGHT]:
                mask += 2
        else:
            if pressed[pygame.K_w]:
                mask += 1
            if pressed[pygame.K_s]:
                mask += 4
            if pressed[pygame.K_a]:
                mask += 8
            if pressed[pygame.K_d]:
                mask += 2
        if SHOT_SPEED[mask] == 0:
            return
        if back_only and not (mask & 8):
                return
        self.collected -= 1
        self.missiles.append(Missile(self.x, self.y, self.id, mask, SHOT_SPEED[mask], SHOT_SPEED[mask], SHOT_W, SHOT_H, self.color))


class Makarona:
    def __init__(self, _x, _y, _side):
        self.x = _x
        self.y = _y
        self.side = _side
        self.color = (100, 100, 0)


def make_makarona():
    saciatok_makarony_x = randrange(WIDTH - MAKARONA_SIDE - 2 * ZONA_W)
    saciatok_makarony_y = randrange(HEIGHT - MAKARONA_SIDE)
    return Makarona(saciatok_makarony_x + ZONA_W, saciatok_makarony_y, MAKARONA_SIDE)


def get_players():
    pozx1 = ZONA_W
    pozx2 = WIDTH - PLAYER_W - ZONA_W
    if randrange(2):
        pozy1 = 0
    else:
        pozy1 = HEIGHT - PLAYER_H
    if pozy1 == 0:
        pozy2 = HEIGHT - PLAYER_H
    else:
        pozy2 = 0
    hrac1 = Player(2, pozx1, pozy1)
    hrac2 = Player(1, pozx2, pozy2)
    return hrac1, hrac2


def main():
    pygame.init()
    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    clock = pygame.time.Clock()
    done = False

    pygame.font.init()
    myfont = pygame.font.SysFont('Comic Sans MS', FONT_SIZE)

    pygame.mixer.music.load('music/best.mp3')
    pygame.mixer.music.play(-1)

    A, B = get_players()
    players = [A, B]
    M = make_makarona()

    off = HRNIEC_H + 1.5 * HRNCE_SPACE
    H1, H2 = [], []
    for center in (off, HEIGHT / 2, HEIGHT - off):
        H1.append(Rectangle(WIDTH - HRNIEC_W, center - HRNIEC_H - HRNCE_SPACE / 2, WIDTH, center - HRNCE_SPACE / 2))
        H2.append(Rectangle(WIDTH - HRNIEC_W, center + HRNCE_SPACE, WIDTH, center + HRNCE_SPACE + HRNIEC_H))

    while True: # not done:
        screen.fill((0, 0, 0))
        pygame.draw.rect(screen, (0, 0, 80), pygame.Rect(WIDTH - ZONA_W, 0, WIDTH, HEIGHT))
        pygame.draw.rect(screen, (0, 0, 80), pygame.Rect(0, 0, ZONA_W, HEIGHT))

        events = pygame.event.get()

        for event in events:
            if event.type == pygame.QUIT:
                done = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    done = True
                if event.key == pygame.K_l:
                    players[1].shoot()
                if event.key == pygame.K_q:
                    players[0].shoot()

        #  Hrnce
        for h1 in H1:
            pygame.draw.rect(screen, players[1].color, pygame.Rect(h1.x1, h1.y1, HRNIEC_W, HRNIEC_H))
        for h2 in H2:
            pygame.draw.rect(screen, players[0].color, pygame.Rect(h2.x1, h2.y1, HRNIEC_W, HRNIEC_H))

        #  Hraci
        for p in players:
            p.move()

        for p in players:
            if collide(
                    Rectangle(p.x, p.y, p.x + p.width, p.y + p.height),
                    Rectangle(M.x, M.y, M.x + M.side, M.y + M.side)):
                if p.collected < MAX_COLLECTED:
                    p.collected += 1
                    M = make_makarona()
                break

        for p in players:
            pygame.draw.rect(screen, p.color, pygame.Rect(p.x, p.y, p.width, p.height))

        for i in range(len(players)):
            for missile in players[i].missiles:
                if collide(players[(i + 1) % 2].rect, missile.rect):
                    players[(i + 1) % 2].collected = max(players[(i + 1) % 2].collected - 3, 0)
                    missile.x = 2 * WIDTH

        #  Strely
        for p in players:
            outdated = []
            for i, missile in enumerate(p.missiles):
                if SHOT_SPEED[missile.direction] == 0:
                    outdated.append(i)
                    continue
                missile.move()
                frika_rect = Rectangle(missile.x, missile.y, missile.x + missile.width, missile.y + missile.height)
                if not collide(frika_rect, Rectangle(0, 0, WIDTH, HEIGHT)):
                    outdated.append(i)
                for h1 in H1:
                    if collide(frika_rect, h1):
                        players[1].score += 1
                        outdated.append(i)
                for h2 in H2:
                    if collide(frika_rect, h2):
                        players[0].score += 1
                        outdated.append(i)
                else:
                    pygame.draw.rect(screen, missile.colour, pygame.Rect(missile.x, missile.y, missile.width, missile.height))
            for o in outdated:
                del p.missiles[o]

        #  Makarona
        pygame.draw.rect(screen, M.color, pygame.Rect(M.x, M.y, M.side, M.side))

        for i, p in enumerate(players):
            if p.score >= MAKARONI_NA_VIHRU:
                txt = myfont.render("Frikadelkova manuna je " + ("CERVENA" if p.id == 1 else "ZELENA"), False, (160, 0, 160))
                r = txt.get_rect(center=(WIDTH / 2, HEIGHT / 2))
                screen.blit(txt, r)
                done = True

        for p in players:
            txt = myfont.render(str(p.collected), False, (0, 206, 209))
            r = txt.get_rect(center=(p.x + p.width / 2, p.y + p.height / 2))
            screen.blit(txt, r)
            txt = myfont.render("{}/{}".format(p.score, MAKARONI_NA_VIHRU), False, p.color)
            screen.blit(txt, (10, 10) if p.id == 1 else (10, HEIGHT - 60))

        pygame.display.flip()

        clock.tick(60)
